package com.yim.david.lunabeetest.data.managers.api

import com.yim.david.lunabeetest.data.entities.UserRemoteEntity
import com.yim.david.lunabeetest.data.services.TechTestService
import io.reactivex.Single

class ApiManagerImpl(private val techTestService: TechTestService): ApiManager {

    override fun fetchUsers(): Single<List<UserRemoteEntity>> = techTestService.fetchUsers()
}