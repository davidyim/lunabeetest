package com.yim.david.lunabeetest.presentation.ui

import android.app.Application
import com.yim.david.lunabeetest.data.di.dataModule
import com.yim.david.lunabeetest.data.di.networkModule
import com.yim.david.lunabeetest.domain.di.domainModule
import com.yim.david.lunabeetest.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LunabeeTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LunabeeTestApplication)
            modules(listOf(
                networkModule,
                dataModule,
                domainModule,
                presentationModule
            ))
        }
    }
}