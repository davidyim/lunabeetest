package com.yim.david.lunabeetest.domain.repositories

import com.yim.david.lunabeetest.domain.models.User
import io.reactivex.Single

interface UserRepository {

    fun fetchUsers(forceRefresh: Boolean): Single<List<User>>
    fun fetchUserById(forceRefresh: Boolean, id: Int): Single<User>
    fun searchUserByName(query: String): Single<List<User>>
}