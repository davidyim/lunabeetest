package com.yim.david.lunabeetest.data.di

import com.google.gson.GsonBuilder
import com.yim.david.lunabeetest.BuildConfig
import com.yim.david.lunabeetest.data.services.TechTestService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { httpLoggingInterceptor() }
    single { okHttpClient(get()) }
    single { retrofit(get()) }
    single { techTestService(get()) }
}


private fun techTestService(retrofit: Retrofit) = retrofit.create(TechTestService::class.java)

private fun retrofit(okHttpClient: OkHttpClient) = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build()

private fun okHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
    .addInterceptor(httpLoggingInterceptor)
    .build()

private fun httpLoggingInterceptor() = HttpLoggingInterceptor().apply {
    if (BuildConfig.DEBUG) {
        level = HttpLoggingInterceptor.Level.BODY
    }
}