package com.yim.david.lunabeetest.domain.usecases

import com.yim.david.lunabeetest.domain.executors.ThreadExecutor
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.repositories.UserRepository
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FetchUsersUseCase(
    private val threadExecutor: ThreadExecutor,
    private val userRepository: UserRepository
) {

    class Params private constructor(val forceRefresh: Boolean) {

        companion object {
            @JvmStatic
            fun withForceRefresh(forceRefresh: Boolean): Params = Params(forceRefresh)
        }
    }

    private var disposable: Disposable? = null


    fun subscribe(params: Params, onSuccess: ((List<User>) -> Unit), onError: ((Throwable) -> Unit)) {
        disposable = userRepository.fetchUsers(params.forceRefresh)
            .subscribeOn(Schedulers.from(threadExecutor))
            .subscribe(onSuccess, onError)
    }

    fun unsubscribe() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
    }
}