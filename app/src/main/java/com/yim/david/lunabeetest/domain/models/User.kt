package com.yim.david.lunabeetest.domain.models

data class User(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val email: String,
    val gender: String,
    val avatar: String
)