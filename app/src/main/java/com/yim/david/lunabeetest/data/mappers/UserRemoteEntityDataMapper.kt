package com.yim.david.lunabeetest.data.mappers

import com.yim.david.lunabeetest.data.entities.UserRemoteEntity
import com.yim.david.lunabeetest.domain.models.User

class UserRemoteEntityDataMapper {

    fun transformRemoteToModel(userRemoteEntity: UserRemoteEntity): User =
        User(
            userRemoteEntity.id,
            userRemoteEntity.firstName,
            userRemoteEntity.lastName,
            userRemoteEntity.email,
            userRemoteEntity.gender,
            userRemoteEntity.avatar
        )

    fun transformRemoteToModelList(forecastRemoteEntityList: List<UserRemoteEntity>): List<User> =
        forecastRemoteEntityList.map { transformRemoteToModel(it) }
}