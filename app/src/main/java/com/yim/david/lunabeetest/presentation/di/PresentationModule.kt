package com.yim.david.lunabeetest.presentation.di

import com.yim.david.lunabeetest.presentation.ui.main.MainActivitySharedViewModel
import com.yim.david.lunabeetest.presentation.ui.main.details.DetailsFragment
import com.yim.david.lunabeetest.presentation.ui.main.details.DetailsFragmentViewModel
import com.yim.david.lunabeetest.presentation.ui.main.list.UserListAdapter
import com.yim.david.lunabeetest.presentation.ui.main.list.UserListFragment
import com.yim.david.lunabeetest.presentation.ui.main.list.UserListFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    single { MainActivitySharedViewModel(get()) }

    scope<UserListFragment> {
        scoped { UserListAdapter() }
        viewModel { UserListFragmentViewModel(get()) }
    }

    scope<DetailsFragment> {
        viewModel { DetailsFragmentViewModel(get()) }
    }

}