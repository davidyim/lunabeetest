package com.yim.david.lunabeetest.presentation.ui.main.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yim.david.lunabeetest.databinding.ItemUserBinding
import com.yim.david.lunabeetest.domain.models.User

class UserListAdapter: RecyclerView.Adapter<UserViewHolder>() {

    private val userList = mutableListOf<User>()
    private var onUserClickListener: (Int) -> Unit = {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(ItemUserBinding
            .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) =
        holder.bind(userList[position], onUserClickListener)

    override fun getItemCount(): Int = userList.size


    fun setUserList(userList: List<User>) {
        this.userList.clear()
        this.userList.addAll(userList)
        notifyDataSetChanged()
    }

    fun setOnUserClickListener(onUserClickListener: (Int) -> Unit) {
        this.onUserClickListener = onUserClickListener
    }
}