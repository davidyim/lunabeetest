package com.yim.david.lunabeetest.data.managers.cache

import com.yim.david.lunabeetest.domain.models.User

interface CacheManager {

    fun saveUserList(userList: List<User>)
    fun getUserList(): List<User>?
}