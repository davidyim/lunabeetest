package com.yim.david.lunabeetest.domain.executors

import java.util.concurrent.*

private const val CORE_POOL_SIZE = 3
private const val MAX_POOL_SIZE = 5
private const val KEEP_ALIVE_TIME_IN_SECONDS = 10L

private const val THREAD_NAME_FORMAT = "android_%d"

class ThreadExecutor : Executor {

    private var threadPoolExecutor: ThreadPoolExecutor? = null

    init {
        this.threadPoolExecutor = ThreadPoolExecutor(
            CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME_IN_SECONDS, TimeUnit.SECONDS,
            LinkedBlockingQueue(), JobThreadFactory()
        )
    }


    override fun execute(runnable: Runnable) {
        this.threadPoolExecutor?.execute(runnable)
    }

    private class JobThreadFactory : ThreadFactory {
        private var counter = 0

        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, THREAD_NAME_FORMAT.format(counter++))
        }
    }
}