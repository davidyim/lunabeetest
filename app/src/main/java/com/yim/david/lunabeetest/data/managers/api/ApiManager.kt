package com.yim.david.lunabeetest.data.managers.api

import com.yim.david.lunabeetest.data.entities.UserRemoteEntity
import io.reactivex.Single

interface ApiManager {

    fun fetchUsers(): Single<List<UserRemoteEntity>>
}