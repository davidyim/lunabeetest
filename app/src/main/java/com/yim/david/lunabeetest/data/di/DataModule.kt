package com.yim.david.lunabeetest.data.di

import com.yim.david.lunabeetest.data.managers.api.ApiManager
import com.yim.david.lunabeetest.data.managers.api.ApiManagerImpl
import com.yim.david.lunabeetest.data.managers.cache.CacheManager
import com.yim.david.lunabeetest.data.managers.cache.CacheManagerImpl
import com.yim.david.lunabeetest.data.mappers.UserRemoteEntityDataMapper
import com.yim.david.lunabeetest.data.repositories.UserRepositoryImpl
import com.yim.david.lunabeetest.data.services.TechTestService
import com.yim.david.lunabeetest.domain.repositories.UserRepository
import org.koin.dsl.module

val dataModule = module {
    single { userRepository(get(), get(), get()) }

    single { apiManager(get()) }
    single { cacheManager() }

    single { userRemoteEntityDataMapper() }
}


private fun userRepository(apiManager: ApiManager,
                           cacheManager: CacheManager,
                           userRemoteEntityDataMapper: UserRemoteEntityDataMapper)
        : UserRepository = UserRepositoryImpl(apiManager, cacheManager, userRemoteEntityDataMapper)

private fun apiManager(techTestService: TechTestService): ApiManager =
    ApiManagerImpl(techTestService)

private fun cacheManager(): CacheManager = CacheManagerImpl()

private fun userRemoteEntityDataMapper() = UserRemoteEntityDataMapper()