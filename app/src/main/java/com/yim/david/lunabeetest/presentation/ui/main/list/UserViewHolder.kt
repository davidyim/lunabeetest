package com.yim.david.lunabeetest.presentation.ui.main.list

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yim.david.lunabeetest.R
import com.yim.david.lunabeetest.databinding.ItemUserBinding
import com.yim.david.lunabeetest.domain.models.User

class UserViewHolder(private val binding: ItemUserBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(user: User, onItemClicked: (Int) -> Unit) {
        with (binding) {
            val context = root.context

            title.text = context.getString(R.string.fragment_list_item_title_format, user.firstName, user.lastName)
            Glide.with(context)
                .load(user.avatar)
                .fitCenter()
                .into(image)

            root.setOnClickListener {
                onItemClicked(user.id)
            }
        }
    }
}