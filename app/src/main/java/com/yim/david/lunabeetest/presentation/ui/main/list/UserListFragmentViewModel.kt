package com.yim.david.lunabeetest.presentation.ui.main.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.usecases.FetchUsersUseCase

class UserListFragmentViewModel(
    private val fetchUsersUseCase: FetchUsersUseCase
) : ViewModel() {

    private val viewStateMutableLiveData = MutableLiveData<UserListFragmentState>()
    private val usersMutableLiveData = MutableLiveData<List<User>>()


    override fun onCleared() {
        fetchUsersUseCase.unsubscribe()
        super.onCleared()
    }


    fun fetchUsers(forceRefresh: Boolean) {
        viewStateMutableLiveData.postValue(UserListFragmentState.LOADING)
        fetchUsersUseCase.subscribe(FetchUsersUseCase.Params.withForceRefresh(forceRefresh),
            {
                usersMutableLiveData.postValue(it)
                viewStateMutableLiveData.postValue(UserListFragmentState.LOADED)
            }, {
                viewStateMutableLiveData.postValue(UserListFragmentState.ERROR)
            }
        )
    }

    fun getViewStateLiveData() : LiveData<UserListFragmentState> = viewStateMutableLiveData
    fun getUsersLiveData() : LiveData<List<User>> = usersMutableLiveData
}