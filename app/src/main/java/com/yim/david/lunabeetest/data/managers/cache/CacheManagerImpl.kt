package com.yim.david.lunabeetest.data.managers.cache

import com.yim.david.lunabeetest.domain.models.User

class CacheManagerImpl : CacheManager {

    private var userList: List<User>? = null

    override fun saveUserList(userList: List<User>) {
        this.userList = userList
    }

    override fun getUserList(): List<User>? = userList
}