package com.yim.david.lunabeetest.domain.usecases

import com.yim.david.lunabeetest.domain.executors.ThreadExecutor
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.repositories.UserRepository
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FetchUserByIdUseCase(
    private val threadExecutor: ThreadExecutor,
    private val userRepository: UserRepository
) {

    class Params private constructor(val forceRefresh: Boolean = false, val id: Int) {

        companion object {
            @JvmStatic
            fun forIdWithForceRefresh(forceRefresh: Boolean, day: Int): Params = Params(forceRefresh, day)
        }
    }


    private var disposable: Disposable? = null


    fun subscribe(params: Params, onSuccess: ((User) -> Unit), onError: ((Throwable) -> Unit)) {
        disposable = userRepository.fetchUserById(params.forceRefresh, params.id)
            .subscribeOn(Schedulers.from(threadExecutor))
            .subscribe(onSuccess, onError)
    }

    fun unsubscribe() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
    }
}