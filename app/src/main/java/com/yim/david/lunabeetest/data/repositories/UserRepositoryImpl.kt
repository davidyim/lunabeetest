package com.yim.david.lunabeetest.data.repositories

import com.yim.david.lunabeetest.data.managers.api.ApiManager
import com.yim.david.lunabeetest.data.managers.cache.CacheManager
import com.yim.david.lunabeetest.data.mappers.UserRemoteEntityDataMapper
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.repositories.UserRepository
import io.reactivex.Single
import java.util.*

class UserRepositoryImpl(
    private val apiManager: ApiManager,
    private val cacheManager: CacheManager,
    private val userRemoteEntityDataMapper: UserRemoteEntityDataMapper
) : UserRepository {

    override fun fetchUsers(forceRefresh: Boolean): Single<List<User>> {
        val cachedUserList = if (forceRefresh) {
            null
        } else {
            cacheManager.getUserList()
        }

        return if (cachedUserList != null) {
            Single.just(cachedUserList)
        } else {
            apiManager.fetchUsers().map {
                val userList = userRemoteEntityDataMapper.transformRemoteToModelList(it)
                cacheManager.saveUserList(userList)
                userList
            }
        }
    }

    override fun fetchUserById(forceRefresh: Boolean, id: Int): Single<User> =
        fetchUsers(forceRefresh).map {
            it.find { user ->
                user.id == id
            }
        }

    override fun searchUserByName(query: String): Single<List<User>> =
        fetchUsers(false).map {
            if (query.isNotBlank()) {
                it.filter { user ->
                    user.firstName.toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault())) ||
                            user.lastName.toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))
                }
            }
            else {
                it
            }
        }

}