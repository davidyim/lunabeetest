package com.yim.david.lunabeetest.presentation.ui.main.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.usecases.FetchUserByIdUseCase

class DetailsFragmentViewModel(private val fetchUserByIdUseCase: FetchUserByIdUseCase)
    : ViewModel() {

    private val userMutableLiveData = MutableLiveData<User>()
    private val errorMutableLiveData = MutableLiveData<Void>()


    override fun onCleared() {
        fetchUserByIdUseCase.unsubscribe()
        super.onCleared()
    }


    fun fetchUserById(forceRefresh: Boolean, id: Int) {
        fetchUserByIdUseCase.subscribe(FetchUserByIdUseCase.Params.forIdWithForceRefresh(forceRefresh, id),
            {
                userMutableLiveData.postValue(it)
            },
            {
                errorMutableLiveData.postValue(null)
            })
    }

    fun getUserLiveData(): LiveData<User> = userMutableLiveData
    fun getErrorLiveData(): LiveData<Void> = errorMutableLiveData

}