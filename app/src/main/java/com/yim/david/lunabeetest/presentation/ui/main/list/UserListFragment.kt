package com.yim.david.lunabeetest.presentation.ui.main.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.yim.david.lunabeetest.databinding.FragmentListBinding
import com.yim.david.lunabeetest.presentation.ui.main.MainActivitySharedViewModel
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserListFragment : ScopeFragment() {

    private var binding: FragmentListBinding? = null

    private val userListFragmentViewModel: UserListFragmentViewModel by viewModel()
    private val mainActivitySharedViewModel: MainActivitySharedViewModel by inject()
    private val userListAdapter: UserListAdapter by inject()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        observeViewState()
        observeUserList()
        observeSearchResult()
    }

    override fun onStart() {
        super.onStart()
        userListFragmentViewModel.fetchUsers(false)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }


    private fun setupViews() {
        binding?.apply {
            recyclerView.apply {
                setHasFixedSize(true)
                val linearLayoutManager = LinearLayoutManager(requireContext())
                layoutManager = linearLayoutManager
                addItemDecoration(DividerItemDecoration(requireContext(), linearLayoutManager.orientation))
                userListAdapter.setOnUserClickListener { id ->
                    findNavController().navigate(
                        UserListFragmentDirections.actionListFragmentToDetailsFragment(id)
                    )
                }
                adapter = userListAdapter
            }

            retryButton.setOnClickListener {
                userListFragmentViewModel.fetchUsers(true)
            }
        }
    }

    private fun observeViewState() {
        userListFragmentViewModel.getViewStateLiveData().observe(viewLifecycleOwner) {
            binding?.apply {
                when (it) {
                    UserListFragmentState.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                        errorText.visibility = View.GONE
                        retryButton.visibility = View.GONE
                    }
                    UserListFragmentState.LOADED -> {
                        progressBar.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                        errorText.visibility = View.GONE
                        retryButton.visibility = View.GONE
                    }
                    else -> {
                        progressBar.visibility = View.GONE
                        recyclerView.visibility = View.GONE
                        errorText.visibility = View.VISIBLE
                        retryButton.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun observeUserList() {
        userListFragmentViewModel.getUsersLiveData().observe(viewLifecycleOwner) {
            userListAdapter.setUserList(it)
        }
    }

    private fun observeSearchResult() {
        mainActivitySharedViewModel.getUsersLiveData().observe(viewLifecycleOwner) {
            userListAdapter.setUserList(it)
        }
    }
}