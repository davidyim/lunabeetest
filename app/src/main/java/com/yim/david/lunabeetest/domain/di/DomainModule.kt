package com.yim.david.lunabeetest.domain.di

import com.yim.david.lunabeetest.domain.executors.ThreadExecutor
import com.yim.david.lunabeetest.domain.usecases.FetchUserByIdUseCase
import com.yim.david.lunabeetest.domain.usecases.FetchUsersUseCase
import com.yim.david.lunabeetest.domain.usecases.SearchUserByNameUseCase
import org.koin.dsl.module

val domainModule = module {
    single { ThreadExecutor() }

    single { FetchUsersUseCase(get(), get()) }
    single { FetchUserByIdUseCase(get(), get()) }
    single { SearchUserByNameUseCase(get(), get()) }
}