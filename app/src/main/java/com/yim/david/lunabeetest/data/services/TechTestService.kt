package com.yim.david.lunabeetest.data.services

import com.yim.david.lunabeetest.data.entities.UserRemoteEntity
import io.reactivex.Single
import retrofit2.http.GET

interface TechTestService {

    @GET("users")
    fun fetchUsers(): Single<List<UserRemoteEntity>>
}