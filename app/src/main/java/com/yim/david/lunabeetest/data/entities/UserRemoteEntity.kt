package com.yim.david.lunabeetest.data.entities

import com.google.gson.annotations.SerializedName

data class UserRemoteEntity(
    @SerializedName("id")
    val id: Int,

    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("gender")
    val gender: String,

    @SerializedName("avatar")
    val avatar: String
)