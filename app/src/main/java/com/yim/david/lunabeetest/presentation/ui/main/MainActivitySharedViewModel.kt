package com.yim.david.lunabeetest.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yim.david.lunabeetest.domain.models.User
import com.yim.david.lunabeetest.domain.usecases.SearchUserByNameUseCase

class MainActivitySharedViewModel(
    private val searchUserByNameUseCase: SearchUserByNameUseCase
) : ViewModel() {

    private val usersMutableLiveData = MutableLiveData<List<User>>()


    override fun onCleared() {
        searchUserByNameUseCase.unsubscribe()
        super.onCleared()
    }

    fun searchUser(query: String) {
        searchUserByNameUseCase.subscribe(SearchUserByNameUseCase.Params.withQuery(query),
            {
                usersMutableLiveData.postValue(it)
            },
            {
                // Do nothing
            })
    }


    fun getUsersLiveData() : LiveData<List<User>> = usersMutableLiveData
}