package com.yim.david.lunabeetest.presentation.ui.main.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.yim.david.lunabeetest.R
import com.yim.david.lunabeetest.databinding.FragmentDetailsBinding
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment : ScopeFragment() {

    private var binding: FragmentDetailsBinding? = null
    private val detailsFragmentViewModel: DetailsFragmentViewModel by viewModel()
    private val arguments by navArgs<DetailsFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBackNavigation()
        observeUserItem()
        observeError()
    }

    override fun onStart() {
        super.onStart()
        detailsFragmentViewModel.fetchUserById(false, arguments.id)
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }


    private fun setupBackNavigation() {
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)
    }

    private fun observeUserItem() {
        detailsFragmentViewModel.getUserLiveData().observe(viewLifecycleOwner) { user ->
            binding?.apply {
                Glide.with(requireContext())
                    .load(user.avatar)
                    .fitCenter()
                    .into(avatar)

                name.text = getString(R.string.fragment_details_name_format, user.firstName, user.lastName)
                email.text = user.email
                gender.text = user.gender
            }
        }
    }

    private fun observeError() {
        detailsFragmentViewModel.getErrorLiveData().observe(viewLifecycleOwner) {
            // Error means argument day was wrong
            navigateUp()
        }
    }

    private fun navigateUp() {
        findNavController().navigateUp()
    }

}