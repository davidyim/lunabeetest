package com.yim.david.lunabeetest.presentation.ui.main.list

enum class UserListFragmentState {
    LOADING,
    LOADED,
    ERROR
}